#Set It Now

Provides call to action to set date field values to 'now'.
To work with entities other than node it requires the Entity (http://drupal.org/project/entity) module.

#Setup:
* (required) Install the module (requirement is the Date module)
* (optional) Provide permission to see the call to action link on the Permissions admin page

#Usage:
* Visit a node view that has at least one Date field
* See the date field has suffix, the call to action link
* Click on it and see the date value is updated, even after refreshing the page
* Try with JavaScript turned off
* Try with a Date field with a to-date
